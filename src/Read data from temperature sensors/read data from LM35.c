#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
int LM35()
{
 float volt_read, temp;
 volt_read = (analogRead(GPIO_PIN_1)/1023)*330; //change it as per needed
 temp = (volt_read*1.8)+32; //From DataSheet
 printf("Temperature: " + temp);
 delay(1);
}
