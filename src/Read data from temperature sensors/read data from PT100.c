#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
int PT100()
{
 float volt_read, temp;
 volt_read = (analogRead(GPIO_PIN_1)/1023)*3.3; //change it as per needed
 temp = volt_read*48.31+106.852; //y=mx+c
 temp = (temp/100-1)/0.00385; //from DataSheet
 delay(1000);
 printf("Temperature: " + temp);
 return(temp);
 delay(1);
}
