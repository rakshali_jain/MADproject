/**  @file   WIFI_config.c
*    @brief  This code stores the WIFI credentials from config.JSON
*            file to the wpa_supplicant folder into
*            the wpa_supplicant.conf file
*
*    @author Akshay Panchal
*    Gitlab - akshaykpanchal
*/

/* ########################################################################################### */
/** Includes */
/** -------- */

/* Standard header files */
#include <stdio.h>
#include <stdlib.h>

/* Library to write/read configuration files */
#include <libconfig.h>

/* Library to parse json file */
#include<json-c/json.h>

/* ########################################################################################### */

/** @brief main function
*
*   This function parse config.json file and writes its
*   output contents to the wpa_supplicant.conf file
*
*   @return success status of writing wpa_supplicant.conf file
*/
int main()
{
FILE *fp; // pointer variable to read file
char buffer[1024]; // buffer to store contents of JSON file
struct json_object *parsed_json; // structure to store entire JSON document
struct json_object *WIFI; // structure to store contents of WIFI
struct json_object *SSID; // structure to store contents of SSID key of WIFI
struct json_object *Password; // structure to store contents of Password key of WIFI

fp = fopen("/etc/shunya/config.json","r"); // pointer to file location in local directory
fread(buffer, 1024, 1, fp); // to read file contents and store it into buffer
fclose(fp); // closes file

parsed_json = json_tokener_parse(buffer); // to parse json file contents and convert it into json object

/** functions to get values of perticular keys of the json file */
json_object_object_get_ex(parsed_json, "WIFI", &WIFI); // to get value of title WIFI
json_object_object_get_ex(WIFI, "SSID", &SSID);  // to get value of key SSID
json_object_object_get_ex(WIFI, "Password", &Password);  // to get value of key Password

/** this is to store values of ssid and passowrd in the variables */
const char* sd = json_object_get_string(SSID); // gets the value of ssid in string form
const char* pd = json_object_get_string(Password); // gets the value of password in string form


static const char *output_file = "/etc/wpa_supplicant/WPA_Supplicant.conf"; // pointer to store the path of the output file
config_t cfg; // describes the configuration
config_setting_t *root, *setting, *group; // describes the settings of configuration

config_init(&cfg); // initialize of the configuration object config
root = config_root_setting(&cfg); // returns the root setting for the configuration config, The root setting is a group

group = config_setting_add(root, "network", CONFIG_TYPE_GROUP); // creates new group network to the root setting

setting = config_setting_add(group, "ssid", CONFIG_TYPE_STRING); // adds the element ssid to the newtowk group
config_setting_set_string(setting, sd); // sets the value of element ssid in string form

setting = config_setting_add(group, "psk", CONFIG_TYPE_STRING); // adds the element psk to the newtowk group
config_setting_set_string(setting, pd); // sets the value of element psk in string form

/** this block will show the result of the execution of writing
    conf file is succeed or not */
if(! config_write_file(&cfg, output_file))
{
    fprintf(stderr, "Error while writing file.\n");
    config_destroy(&cfg);
    return(EXIT_FAILURE);
}

fprintf(stderr, "New configuration successfully written to: %s\n",
          output_file);

config_destroy(&cfg); //initialize and destroy the configuration object config
return(EXIT_SUCCESS); // exit function to indicate successful program completion
}
