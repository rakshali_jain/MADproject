import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP 
from adafruit_mcp3xxx.analog_in import AnalogIn

# Using MCP3008 ADC

spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI) # create the spi bus
cs = digitalio.DigitalInOut(board.D22) # create the cs (chip select)
mcp = MCP.MCP3008(spi, cs) # create the mcp object
value = AnalogIn(mcp, MCP.P0) # create an analog input channel on pin 0

def remap_range (in_volt, in_min, in_max, out_min, out_max):
  in_span = in_max - in_min
  out_span = out_max - out_min
  
  return (out_min + (in_volt * out_span / in_span))

while True:
  in_volt = value
  #Assuming that a 12Volts operated battery is used
  battery_volt = remap_range(value, 0, 65535, 0, 12)
  #if not then change 12 to the maximum voltage of the battery used
  print('Battery Voltage = ', battery_volt)
  time.sleep(0.5)
