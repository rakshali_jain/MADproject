## CODE FOR READING VALUES OF SENSORS

This code is implemented for reading the values of sensors such as accelerometer(three axis), temperature, sound and time.

- The code is implemented using structure.
- The `struct` is used to store all the data collected from the sensors.
- Because of this `struct` we can call only one function in the main function.
- The function read and printing the values from sensors is separately implemented as a function.
- `delay` can be changed according to the sensor type.

### API

- shunyaInterfaces
- Docker

### OS

- Ubuntu 16.04

### COMPILING CODE

- Create new document with extension `.c` 
- Code in that document and save it.
- Open command line.
- Install Docker, if not installed.
- Install shunyaOS (use 0.2 tag) [Download shunyaOS](https://hub.docker.com/r/shunyaos/shunya-armv7)
- Use this command for accessing container, `sudo docker run -v /home/...(your_code_file_directory_location).../:/src/...(any_name)... -ti shunyaos/shunya-armv&:0.2 /bin/bash`
- After this use, `cd /src/...(any_name)...`
- After using these above command, you can compile your document with code.
- Compile with:
`gcc -o (..any_name..) (..file_name..).c  -lshunyaInterfaces_core -lshunyaInterfaces_user -I/usr/ -L /usr/`
- If there is error or compilation terminated, means file doesn't compiled successfully.
- If there is no error or warning, means file compiled successfully.

### Output

The output consists of the variables that is present in the print function.

For this code, it gives 6 values as output. 
- accelerometer_x
- accelerometer_y
- accelerometer_z
- temperature
- sound_level
- time

### Errors faced:

- Don't use `const int MPU6050_addr = 0x68;` inside the main function because it gives as MPU6050_addr as not declared.
- Don't use <struct sensors `read`(void)>, because the declaring library doesn't consider read as a variable. Instead use any other variable.
