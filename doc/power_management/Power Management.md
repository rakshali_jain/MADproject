## **BATTERY CHARGE DETECTION**

#### **Principle: Voltage Divider Rule** 
In the field of electronics, a voltage divider is a basic circuit, used to generate a part of its input voltage like an output. This circuit can be designed with two resistors otherwise any passive components along with a voltage source. The resistors in the circuit can be connected in series whereas a voltage source is connected across these resistors. This circuit is also called a potential divider. The input voltage can be transmitted between the two resistors in the circuit so that the division of voltage takes place.

![](doc/power_management/VDR.png)

#### **Methods to Apply Voltage Divider Rule -**
1.	Voltage Detection Sensor:
Voltage Sensor is a precise low-cost sensor for measuring voltage. It is based on principle of resistive voltage divider design. It can make the red terminal connector input voltage to 5 times smaller. Arduino analog input voltages up to 5V, the voltage detection module input voltage not greater than 5Vx5=25V (if using 3.3V systems, input voltage not greater than 3.3Vx5=16.5V).

![](doc/power_management/VDS.png)

2.	Making own Sensor: 
Take two resistors, one resistor of (VOLT x 1000) / 3 where VOLT represents the base voltage applied by the battery and another resistor to be three times the resistance of the first resistor. Now place both the sensors in series with the two terminals of the battery and analog input pin and GND across the resistor with lesser resistance.
Now, by studying the datasheet of the battery being used, we can determine the charge left on the battery varying from 0V to 3V.

#### **Advantages of using Voltage Divider Rule:-**

1. Simplest method to measure voltage by dividing and measuring small proportion if larger voltage
2. Main voltage is many times too high to be directly measured, when this method is handy and convenient

#### **Disadvantages of using Voltage Divider Rule:-**
1. Measurement only as accurate as the components used for dividing the voltage, which introduce their own uncertainty.
2. If resistance dividers are used, they introduce a small power loss.
3. The ratio may be again affected by voltmeter resistance, which adds up to uncertainty.
4. If capacitor dividers are used, tolerances and individual values have to be very close.


#### **Additional Information** 
As raspberry pi has only digital input and output so we need o use ADC(Analog to digital converter) to convert analog signals into digital signals. 
This is ADS1115 16-Bit ADC – 4 Channel with Programmable Gain Amplifier.

![](doc/power_management/ADC.png)

For micro-controllers without an analog-to-digital converter or when you want a higher-precision ADC, the ADS1115 provides 16-bit precision at 860 samples/second over I2C. The chip can be configured as 4 single-end input channels or two differential channels. Interfacing is done via I2C. The address can be changed to one of four options (see the datasheet table 5) so you can have up to 4 ADS1115’s connect on a single 2-wire I2C bus for 16 single end inputs.
#### Features of ADS1115 16-Bit I2C ADC 4-Channel Programmable Gain Amplifier Module:- 
1. WIDE SUPPLY RANGE: 2.0V to 5.5V
2. PROGRAMMABLE DATA RATE: 8SPS to 860SPS
3. It has an Internal low-drift voltage reference
4. Internal Oscillator.
5. Internal PGA.
6. I2C(Inter-Integrated Circuit) INTERFACE : Pin-Selectable Addresses.
7. Four single ends Or Two differential inputs.
8. Programmable Comparator.
9. This board/chip uses I2C 7-bit addresses between 0x48-0x4B, selectable with jumpers
10. LOW CURRENT CONSUMPTION: Continuous Mode: Only 150µA Single-Shot Mode: Auto Shut-Down

#### **Advantages of Analog to Digital Converter(ADC):-**
1. Flash ADCs are the fastest compared to the other Analog to Digital Converter.
2. Compared to other converters, Sigma Delta ADCs offer high resolution at low-cost.
3. Successive Approximation ADCs operate at high speed and are more reliable.
4. Sigma-Delta ADCs have higher noise shaping capability and also offer high resolution.
5. Pipelined ADCs also offer high resolution at high speed.

#### **Disadvantages of Analog to Digital Converter(ADC):-**
1. Circuit Complexity increases with the increase in the use of Comparators in Flash ADCs.
2. Flash ADCs are expensive.
3. Converting non-periodic signals using Pipeline ADCs can be difficult as it typically runs at a periodic rate.
4. Pipeline ADCs are sensitive to board layout.
5. Pipeline latency of the input signal occurs in Pipeline ADCs which causes non-linearities in the parameters like Offset and Gain.
6. Successive Approximation converters used for higher resolution will be slower.
